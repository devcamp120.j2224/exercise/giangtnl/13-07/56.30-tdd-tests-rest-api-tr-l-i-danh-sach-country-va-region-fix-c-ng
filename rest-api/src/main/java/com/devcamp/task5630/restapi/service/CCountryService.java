package com.devcamp.task5630.restapi.service;
import java.util.ArrayList;
import java.util.List;
import com.devcamp.task5630.restapi.model.CCountry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class CCountryService {
    private static List<CCountry> countryList = new ArrayList<CCountry>();
    @Autowired
    static CRegionService regions;
    static {
        CCountry vietnam = new CCountry("Việt Nam", 2021, null);
        CCountry us = new CCountry("Hoa Kỳ", 2221, null);
        CCountry russia = new CCountry("Nga", 2321, null);
        countryList.add(vietnam);
        countryList.add(us);
        countryList.add(russia);
        for (int i = 0; i < countryList.size(); i++){
            if (countryList.get(i).getCountryName() == "Việt Nam"){
                countryList.get(i).setRegions(CRegionService.getRegionVietnam());
            } else if (countryList.get(i).getCountryName() == "Hoa Kỳ"){
                countryList.get(i).setRegions(CRegionService.getRegionUs());
            } else if (countryList.get(i).getCountryName() == "Nga") {
                countryList.get(i).setRegions(CRegionService.getRegionRussia());
            }
        }
    }
    public CCountryService(){
        super();
    }
    public static List<CCountry> getCountryList() {
        return countryList;
    }
    public static void setCountryList(List<CCountry> countryList) {
        CCountryService.countryList = countryList;
    }
    


}
