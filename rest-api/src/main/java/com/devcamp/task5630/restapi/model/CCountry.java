package com.devcamp.task5630.restapi.model;
import java.util.List;
public class CCountry {
    private String countryName;
    private int countryCode;
    private List<CRegion> regions;
    public CCountry(String countryName, int countryCode, List<CRegion> regions) {
       super();
       this.countryName = countryName;
       this.countryCode = countryCode;
       this.regions = regions; 
    }
    public CCountry(){
    }
    public String getCountryName() {
        return countryName;
    }
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
    public int getCountryCode() {
        return countryCode;
    }
    public void setCountryCode(int countryCode) {
        this.countryCode = countryCode;
    }
    public List<CRegion> getRegions() {
        return regions;
    }
    public void setRegions(List<CRegion> regions) {
        this.regions = regions;
    }
    
}
